📣 Convite para Reunião do Movimento Software Livre Brasil

🗓️ Data e Hora: 14 de junho, das 19h às 21h

🌐 Local: https://jitsi.biglinux.com.br/PlenariaReuniaoSL14J (sala de conferência)
(FYI: esse jitsi tá atrás de cloudflare, não deixa qualquer um passar, particularmente quem usa tor e não deixa qualquer um rodar web blobs no seu navegador; vou assistir com mpv https://invidious.snopyta.org/watch?v= EXWsosztJVs -lxo https://invidious.snopyta.org/watch?v= udt9MOQ0kmQ é a mesma sessão? tá muito curta)  

📺 Transmissão ao vivo: https://tv.taina.net.br/ ou https://youtube.com/@SoftwareLivreBrasil


Apresentação
A comunidade Software Livre no Brasil formulou e está promovendo uma proposta dentro do Processo de Participação no PPA do Governo Federal . Este esforço faz parte de uma campanha nacional para fortalecer a presença do software livre em nosso país. 

Para apresentar e promover nossa proposta, vamos realizar uma reunião online, com as presenças já confirmadas de Sérgio Amadeu , Nina da Hora , Jader Gama, Livia Ascava e muitas outras. (veja a lista completa ao final do documento)

Vale dizer que esse encontro também marcará o início de uma nova fase de articulação do Movimento do Software Livre no Brasil, com o anúncio de muitas novidades e parcerias nacionais e internacionais


== Pautas da Reunião ==

1. Apresentação da Comunidade
As fundadoras da comunidade apresentam o histórico e contextualizam o processo de organização que levou à criação do grupo Comunidade Software Livre no Telegram (http://t.me/ComunidadeSoftwareLivre), e de lá pra cá, quais foram os passos para a mobilização que provocou a reunião de hoje.

    Fabs Balvedi e Lívia Gouvêa - 15min

    Apresentação de Ferramentas e Cerimônias de Governança para a Comunidade


2. Detalhamento da Campanha Software Livre do PPA!
Apresentação do histórico e do processo de elaboração e escolha da proposta que a comunidade Software Livre elegeu para promover dentro do Processo de Participação no PPA do Governo Federal: http://bit.ly/SoftwareLivreNoPPA

    Uirá Porã e Laila Bellix - 15min

    Apresentação e convite para construir a proposta do PPA: https://pad.riseup.net/p/ServicoDigitalBrasileiro-keep


3. Representantes do Movimento Software Livre Brasil
Queremos nos rever, nos ouvir, discutir a agenda, políticas, projetos e eventos do movimento Software Livre no Brasil, além de planejar como podemos trabalhar juntos para fortalecer a soberania digital. 

Mas como seremos muitas pessoas, estamos sugerindo iniciar a conversa garantindo falas sobre a proposta do PPA, para algumas pessoas  representativas, tentando contemplar ao máximo critérios de equilíbrio e diversidade. São elas:

    Sergio Amadeu - UFABC - SP (confirmado)

    Nina da Hora - IDH - RJ (confirmada)

    Jader Gama - CosmoTécnicas Amazônicas - PA (confirmado)

    Renata Gusmão - ThoughtWorks - PE (confirmada)

    Sady Jacques - ASL / FISL - RS (confirmado) 

    Gecíola Fonseca - Secult - CE (confirmada)

    Corinto Meffe - Comunidade Software Público - RJ (confirmado)

    Livia Ascava - Hacklab/ - SP (a confirmar)


No começo da reunião, vamos disponibilizar o link de um PAD e pedir para que todas as pessoas acessem e coloquem seus Nomes, Instituições, coletivos e/ou movimentos que fazem parte, assim como iniciativas ou projetos que tenham executado, estejam executando ou queiram executar, com seus respectivos links.

4. Planejamento da próxima reunião: 
Vamos discutir a realização de uma nova reunião, desta vez presencial, em agosto.


Junte-se a nós nesta reunião para participar dessas discussões e contribuir para a retomada do Movimento Software Livre Brasil. 


Aguardamos a sua presença!


Pessoas Confirmadas:
(inclua seu nome, instituição e UF ou País em que atua)

    Jader Gama - CosmoTécnicas Amazônicas - PA

    Gecíola Fonseca - SecultCE - CE

    Sergio Amadeu - UFABC/CGI - SP

    Lívia Ascava - hacklab/

    Uirá Porã - Instituto Mutirão / Mídia Ninja - CE

    Fabiane Balvedi - estudiolivre.org - PR

    Marcelo Branco - RS

    Kelly Arruda - Ministério da Saúde - CE

    Cláudio Prado - SP

    Ricardo Poppi - Instituto Cidade Democrática

    João Paulo Mehl - SoyLoco - PR

    Murilo Machado - OKBR - SP

    Erica B. Pinho - FeliciLab - CE

    Clara Costa - PA

    Frederico G. Guimarães - Vilarejo - MG

    Everton Rodrigues - Pindamonhamgana - SP

    Hadrien Froger — Octree.ch / voca.city - Suíça

    Tiago Bugarin - Clube Raul Hacker - BA

    Higor Passos - ES

    Júlio Neves See More

    Vince Tozzi - Casa de Cultura Tainã / Rede Mocambos

    pBaesse - IFRN / potilivre.org - RN

    Pedro Markun - LabHacker/SP

    Felipe Bombardelli - PCO - PR

    Diego Rojas - Inst. Pombas Urbanas - SP

    zé - Taubaté/SP

    Edu Agni - SP

    Dickson Melo - RN

    Pedro Lara Campos - Chaotic/Garuda - SP

    Matheus Oliveira Braz - UEMS - MS

    Corinto Meffe - BR

    Henrique Z. Parra - Pimentalab/Lavits - Univ.Federal de São Paulo

    Rickson Figueira - Greenpeace Brasil / SP

    Rodrigo Duarte - SP

    Tito Faria - SP

    Fernando Henrique - UFPB

    Jefferson Falcão - Cultura do MS

    Ian Fernandez - SP

    Cinco Euzebio - RJ

    Leonardo Foletto - BaixaCultura/SP

    Adriano Faria - sp See More

    Mariel Zasso - Red f<a+i>r - México 

    Sérgio Ramos - GDG Sinop - MT

    Alex Rocha - Pref.Peixoto de az. -MT

    Francisco Monaco - Centro FOSS USP/SP

    ​​Tiago J. Ferreira - IFSC 

    Luan da Costa Redmann — IFRN, PotiLivre, Potigol

    Diego Dorgam - Mapeo Brasil

    Emerson Teles- IFRN

    Isabella Moreira - Unisol Bahia  

    Hilquias Abias Figueiredo Silva - IFRN

    Valéria Barros - SP

    Ygor Silva Santos - UNIVESP - SP

    Rodrigo Vieira - Parahyba

    Rafael Roxo Rodrigues - Movimento Brasil Popular

    Juliano JS Tavares - coopera.dev.br

    Daniel Lenharo de Souza - ICTL/Debian Brasil/Comunidade Curitiba Livre - Curitiba/PR

    Raul Amorim - MST - SP

    Paulo Henrique de Lima Santana - ICTL/Debian Brasil

    Juliana Trevine - EACH/USP - SP

    Pedro Braga - ITS Rio - RJ

    Renato Ribeiro - Universidade Federal do Ceará

    Mário Sérgio - Presidência da República - DF

    Fátima Maia - Avós da Terra - Norte MG 

    Alan Veloso - Universidade Federal do Pará - PA

    Paulo Victor Loureiro - SEDUCCE

    Martina Tortelli - Paraná 

    Douglas Nunes - ESDI - RJ

    Lucas - Mutirão - SP

    Brendo Freitas - RJ

    Ivan Rodrigues - 

    Carla Rocha - Unb - DF

    Josir Cardoso Gomes - IBICT/UFRJ - RJ

    Claudio Alfonso - ParaLivre.org / LabLivre.org - Belém - PA

    José Roberto da Costa Ferreira - ParaLivre.org - Comunidade Paraense de Software Livre - Belém - PA

    Jacqueline Cristina Teixeira do Rosário - ParaLivre.org - Comunidade Paraense de Software Livre - Santa Izabel - PA

    Aluizio Neto - BH Livre - Belo Horizonte/MG

    Melissa Mendonça - Quansight/NumPy/SciPy - Florianópolis/SC

    Tarcísio Lemos Monteiro Carvalho / IFPA, Comunidade Paralivre Paragominas-PA

    Valcilon Silva - Brasília, DF 

    Lenon Andrade (Wilkens) /

    Stella Paterniani - Unesp 

    Bruna Graça - IPPUR/UFRJ - Niteroi, RJ

    Lucas Queiroz - Dataprev - CE

    Jeferson Fernando - LINUXtips - Brasil

    Ilara Hämmerli- Fiocruz e Abrasco '

    Giovani Balarini - ES/Vitória

    Eduardo Lima - Instituto Intercidadania/ Rede das Produtoras Culturais Colaborativas / GT Cultura Digital CNPdC / Produtora Colabor@tiva.PE

    Marcos Mazoni-FGV 

    Fabio Araujo - SP

    Antonio Arles - MG

    Alexsandro Carvalho (alexsandroccarv) STI/Universidade Federal de São Paulo

    Evelyn Gomes - LabHacker - AL

    Filipe Saraiva - KDE, CCSL/UFPA, Educação Vigiada - PA

    Mariana Antoun - MediaLab.UFRJ - RJ

    Anápuàka M. Tupinambá Hã hã háe - Rádio Yandê, Cultura Digital Indígena e Etnomidia Indígena - Brasil 

    Thiago Skárnio - Alquimídia - SC

    saotome - USP - SP

    Rogério Machado - SP, união da juventude comunista

    Mariana Ramos - UFRJ - RJ

    Camilo Cunha de Azevedo - Universidade Brasileira Livre - RS

    Rodrigo Campos - USP, militante do MLB (Unidade Popular pelo Socialismo)

    Luis Soeiro - DF

    Geraldo Amaral - DF

    Aurélio Heckert - BA

    Thiane Neves - Rede Transfeminista de Cuidados Digitais - PA

    Cláudio Brandão (Cráudio) - Debian MG /  Rede de Produtoras Culturais Colaborativas / BH Livre / Áudio e Software Livre - MG

    Wladimir Crippa - bitconf - SC

    Asa - Poiesis Tecnologia - SP

    Nelson Pretto - UFBA/ Universidade de Barcelona [por conta do horário, estou 5 horas na frente, só devo aparecer no começo! Mas tô colado!]

    Rafael Chaves Freitas - hacklab/

    Janaína Diniz - UEMG - 

    Alexandre Oliva - FSFLA - SP

    Evandro Schaulet - RS

    Capi Etheriel / labhacker sp

    Leo Val de Casas (digid) - RJ

    Marco Fonseca - SP

    Frederico Gonçalves Guimarães - Vilarejo - MG

    Antonio Coe1lho - ColetivoFarpa.org - BA

    Marcello de Souza - LivreLabs - CE

    Nilton Felipe Braz - Uninassau - CE

    Olivier Hallot (The Document Foundation - LibreOffice) - RJ

    Ricardo Oliveira -  técnico informática autônomo 

    Paulo Kretcheu - SP

    Fernanda Hoffmann Lobato

    Farid Abdelnour - Estúdio Gunga / Kdenlive - DF

    Camilo Micheletto - F1rst - SP

    Pedro Jatobá - Universidade Livre da Chapada Diamantina / Cooperativa EITA / Rede das Produtoras Culturais Colaborativas - Chapada Diamantina / BA

    Carlinhos Cecconi - continuamente.com.br 

    Ronaldo José da Silva - PE

    M. Toledo - Instituto Aaron Swartz

    Nina da hora - instituto da hora  

    Teodoro Colombo - UTFPR Toledo

    Luis Felipe Murillo - Mov. Software Livre, RS

    Cesar Fuentes - Toledo/

    Cristiano Furtado - Lauro de Freitas - BA / Projeto Liberte-se 

    Rafael Ruscher - BigLinux - Brasília - DF

    Cristina Ribas - Porto Alegre/RS - i-motirõ

    Rafael Gomes (Gomex) - Salvador/BA - Raul Hacker Club

    Matheus Gimenez - Bragança Paulista, SP

    Larissa Milhorance - AI Girls, Mov. Juventudes - Belford Roxo, RJ

    Fábio Trentino de Souza - São Carlos, SP

    Sérgio Monteiro - Recife, PE

    Pedro Carneiro, BA

    Antonio Terceiro - ICTL/Debian - BA

    Nilton Felipe Braz - Ceará 

    Ronaldo Silveira - São Paulo - SP

    Rafael Wild - UTFPR-FB - PR

    Barnabé di Kartola - XIVA Studio/BigLinux - BR

    Bruno Gonçalves - BigLinux - DF

    Edson Conceição da Silva - SP

    Chico Caminati - UNESP / WEDE'RÃ LAB / Rede Mocambos - Presidente Prudente, SP 

    Tales A. Mendonça - JF

    Junior Paixao - Socializando Saberes / Tainã / Rede Mocambos

    Bruno Ribeiro - NY

    Viviana Garrido /UFAM - Manaus/AM

    Alan Martins - Alto Tietê/SP 

    Rodrigo Gaete - Ministério da Saúde/SAPS

    Lenilson Jose Dias - Sao Paulo - SP

    Andre Formiga - Porto Alegre RS

    Vanessa Me Tonini - Marialab/USP/TW- SC

    Neto Muniz - Fortaleza/Ceará 

    Bruno Almeida - Jacareí/SP

    Mariana Tamari - São Paulo - Coding Rights

    Cesar Brod - Lajeado, RS - BrodTec

    Lucs Teixeira - Rio de Janeiro / RJ - Mycelium / Criptofunk

    Lucas santos-SP

    Edvaldo siqueira - São José dos Campos -SP

    Hugo Guarilha - Ouro Preto - Minas 

    Camilla de Godoi - EITA / UFRJ - MG / RJ

    Eduardo Vieira Barbosa - Brasília - DF

    Rodrigo Canalli - STF

    João Fernando Costa Júnior - BH/MG

    Thiago Pezzo - Debian MG / IFSULDEMINAS 

    Ronualdo Maciel - NaRN

    Vítor Capparelli - São Paulo - SP

    Cacau Arcoverde - Web Rádio Catimbau/Coletivo conexão Catimbau - Rede.PE /GT Latino Americano CNPdC 

    Renan Felipe Brito Dantas - Univ. Federal do Vale do São Francisco 

    Rodinei Costa - ParaLivre.org / LabLivre.org / Federação Estadual de Entidades Comunitárias do Pará (Fecpa) - Ananindeua - PA

    Jeison Pandini - Criciúma/SC

    Bruno Venâncio - São Paulo/SP

    Emerson Sachio Saito /Demoiselle/Curitiba Livre - PR

    aromab

    Luiz Sanches - Cosmotécnicas Amazônicas - PA

    Marco Pinheiro - PA

    Sílvia Maria de Paiva-Ong Aicc CE

    Guilherme - Matehackers - RS

    Sady Jacques 

    Pontal Sereuwazaowe Xavante - Wede'rã Lab - São Carlos - SP

    Josimar - USP - SP 

    Edu Aguiar - Maranhão

    Ângelo Barbosa - RN, militante da União da Juventude Comunista (UJC)

    Pablo Etcheverry - Moodle - Argentina

    Carlos Machado - ASL - Brasil

    Cristiano Therrien - Université de Montréal / University of Ottawa

    Gustavo Pacheco (The Document Foundation/LibreOffice) - RS

    Caio Messias - SP

    Andréa Saraiva Martins - Pesquisadora WASH/CNPq



COMENTÁRIOS DE PESSOAS QUE PASSARAM POR AQUI:


OBS: É contraditório marcar uma reunião de software livre desse peso e agendá-la com um link do Google Calendar escondido atrás de um link bit.ly. (Geraldo Amaral) +3 Em relação a isso, o Nextcloud do Vilarejo está disponibilizado para o grupo e agendamentos podem ser feitos lá. Basta compartilhar o link e ele pode ser acessado por outros softwares/serviços de agendamento.

Sugestão de plataformas para reunião (afinal, é sobre software livre e não faz sentido utilizar plataformas proprietárias de bigtechs ou não que rastream dados, certo?):
    1. Chat via rede Matrix, livre, segura, federada ( https://matrix.org )
    2. Vídeo via Jitsi meet, livre, sem login ( https://meet.jit.si ) +5
    3. O mumble aguenta tudo isso de gente? Seria um espaço muito bom (se todo mundo não falar ao mesmo tempo, sim).
    4. O BigLinux oferece o servidor com Jitsi Meet ( https://jitsi.biglinux.com.br ) para as reuniões de vídeo. Obs: O servidor aguenta, já fizemos outras reuniões.
    6. Tainã/Rede Mocambos/SIWAZI@UNESP/Socializando Saberes: Oferecemos a TV Taina para transmissão e gravação a partir de uma Sala Jitsi (ou OBS ou outra plataforma que aceite protocolos RTMP/RTMPS/SRT): https://tv.taina.net.br  (Orientações: https://nuvem.taina.net.br/index.php/s/Configuracao-Transmissao-TVTaina)
    7. Tainã/Rede Mocambos/SIWAZI@UNESP/Socializando Saberes: Temos, também, duas plataforma Jitsi com opção habilitada para fazer transmissões ao vivo para diversas plataformas que trabalhem com protocolo RTMP/RTMPS/SRT:
        https://meet.taina.net.br/
        https://siwazi.fct.unesp.br:8443/
    8. Tainã/Rede Mocambos/SIWAZI@UNESP/Socializando Saberes: Temos, também, uma plataforma Nextcloud:
        https://nuvem.taina.net.br
    9. Tainã/Rede Mocambos/SIWAZI@UNESP/Socializando Saberes: Temos um canal no Telegram lincado com as transmissões ao vivo da TV Taina: https://t.me/s/TVTainaCanal (toda transmissão iniciada é imediatamente anunciada nesse canal).
    10. Jami - software livre P2P "peer to peer"  ( https://jami.net/ )


A instância "central" do Jitsi dificilmente aguentaria tanta gente; já vi gente tentar uma reunião com 150 pessoas lá e não rolou.  Mumble é bem mais leve, por ser só áudio, capaz que funcione.  Um BigBlueButton pode aguentar esse volume numa instância bem parruda, mas a da FSF, por exemplo, não recomenda mais de 50 pessoas numa sessão.  Matrix acho complicado porque exige captcha privativo pra abrir conta na maior parte das instâncias.  Será que tentamos algo P2P?  GNU Jami tem suporte a conferências, inclusive com vídeo, roda basicamente em qualquer computador de mesa, de colo e de mão, mas nunca fiz com tanta gente assim. -lxo

Como não vamos falar todos ao mesmo tempo, que tal coordenanar pelo grupo de msg instantânea e a medida que alguém for falar entrar na sala?
A sala poderia ser retransmitita por alguma plataforma separada, assim vamos assistindo e se necessário a pessoa que vai falar entra e outra sai.

Sobre o limite de participantes na conferência, poderiamos tentar N breakout rooms no Jitsi, enquanto na sala principal fica só quem está na fila para falar. A transmissão da sala principal pode ser compartilhada com as outras salas via compartilhamento de tela. N pessoas serão responsáveis por uma breakout room. Essas pessoas ficarão na sala principal com uma aba e com outra ficará na breakout room compartilhando a tela.

Áudio também? Sim, compartilhamanto de tela pode compartilhar áudio. No Chromium isso se dá automaticamante ao compartilha aba, no Firefox é possivel usar um mic que representa o audio interno.

PRÉ-EVENTO (SUPORTE TÉCNICO): Pessoal, Vamos tentar validar as propostas tecnológicas para a conferência nesta Terça-Feira 19h? (Jitsi, Jami, Mumble...) Eu (aurium) e Oliva (lxo) estamos no Jami. Pode me encontrar no Telegram tb. Me chame e vamos fazer alguns testes.


Hubs de Participação Remota
O Felipe "Juca" Sanches deu uma idéia legal, de organizar espaços em cada cidade, onde poderíamos participar coletivamente, usando apenas uma conta. Isso não impede participem sozinhos os que não puderem/desejarem encontrar o grupo local.

    Bahia

    Salvador: LASID, sala 159, no Instituto de Matematica e Estatistica da UFBA

    São Paulo

    São Paulo: ...



Tiago Bugarin: Concordando com o espírito da observação acima editei o texto pra ficar mais claro e ser mais coerente (arquivo ICS) sem deixar de dar a opção do Google Calendar pra quem quiser, sendo explicito.
Pras sugestões de plataforma: já existe uma sala na Matrix com uma ponte pro grupo no Telegram; e há uma discussão em andamento não sobre qual software de conferência será usado mas sobre qual instância vai guentar tanta gente. cheguem na sala pra nos ajudar.

Matrix: https://matrix.to/#/#comunidadesoftwarelivre:matrix.org
Telegram: https://t.me/ComunidadeSoftwareLivre
XMPP: https://movim.mocambos.net/?login/AGqzWXyy
GNU Jami: ??? (veja abaixo)+1
Tox: ???

Vamos parar de passar vergonha com sistemas centralizados ou semi-centralizados que induzem à dependência de sistemas não livres?  Criei um grupo chamado Comunidade Software Livre Brasil no GNU Jami (https://jami.net pra instalar o cliente P2P), mas não sei como compartilhar aqui o endereço dessa comunidade.  Quem quiser entrar manda mensagem pra mim no Jami (minha conta é lxo).  - Alexandre Oliva
E eu sou "aurium" no Jami - Aurélio Heckert

  ,= ,-_-. =.
 ((_/)o o(\_))
  `-'(. .)`-'
      \_/ GNU/Linux 
  .---.
      /     \
      \.@-@./
      /`\_/`\
     //  _  \\
    | \     )|_
   /`\_`>  <_/ \
jgs\__/'---'\__/

































